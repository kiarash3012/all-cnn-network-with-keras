from keras.datasets import cifar10
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dropout, Activation, Conv2D, GlobalAveragePooling2D
from keras.optimizers import SGD
from matplotlib import pyplot as plt
import numpy as np


seed = 6
np.random.seed(seed)
(x_train, y_train), (x_test, y_test) = cifar10.load_data()

for i in range(0, 9):
    plt.subplot(330 + 1 + i)
    img = x_train[i]
    plt.imshow(img)

# show the plot
plt.show()

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')

x_train /= 255.0
x_test /= 255.0

y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)
x_test = np_utils.to_categorical(x_test)
num_class = y_test.shape[1]

x_test = x_test[:, :, :, :, 0].transpose([0, 3, 2, 1])


def all_cnn(weights=None):
    _model = Sequential()
    _model.add(Conv2D(96, (3, 3), padding='same', input_shape=(3, 32, 32)))
    _model.add(Activation('relu'))
    _model.add(Conv2D(96, (3, 3), padding='same'))
    _model.add(Activation('relu'))
    _model.add(Conv2D(96, (3, 3), padding='same', strides=(2, 2)))
    _model.add(Dropout(0.5))
    _model.add(Conv2D(192, (3, 3), padding='same'))
    _model.add(Activation('relu'))
    _model.add(Conv2D(192, (3, 3), padding='same'))
    _model.add(Activation('relu'))
    _model.add(Conv2D(192, (3, 3), padding='same', strides=(2, 2)))
    _model.add(Dropout(0.5))
    _model.add(Conv2D(192, (3, 3), padding='same'))
    _model.add(Activation('relu'))
    _model.add(Conv2D(192, (1, 1), padding='valid'))
    _model.add(Activation('relu'))
    _model.add(Conv2D(10, (1, 1), padding='valid'))

    # global avg polling layer (softmax)
    _model.add(GlobalAveragePooling2D())
    _model.add(Activation('softmax'))

    if weights:
        _model.load_weights(weights)

    return _model


learning_rate = 0.01
weight_decay = 1e-6
momentum = 0.9

model = all_cnn()

sgd = SGD(lr=learning_rate, decay=weight_decay, momentum=momentum, nesterov=True)
model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
print(model.summary())

epochs = 350
batch_size = 32

model.fit(x_train.transpose([0, 3, 2, 1]), y_train, validation_data=(x_test, y_test), epochs=epochs,
          batch_size=batch_size, verbose=1)


scores = model.evaluate(x_test, y_test, verbose=1)
print('Accuacy of AllCnn: {}'.format(scores[1]))

classes = range(0, 10)

names = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']

# zip the names and classes to make a dictionary of class_labels
class_labels = dict(zip(classes, names))

# generate batch of 9 images to predict
batch = x_test[100:109]
labels = np.argmax(y_test[100:109], axis=-1)

# make predictions
predictions = model.predict(batch, verbose=1)

class_result = np.argmax(predictions, axis=-1)

# create a grid of 3x3 images
fig, axs = plt.subplots(3, 3, figsize=(15, 6))
fig.subplots_adjust(hspace=1)
axs = axs.flatten()

for i, img in enumerate(batch):
    for key, value in class_labels.items():
        if class_result[i] == key:
            title = 'Prediction: {}\nActual: {}'.format(class_labels[key], class_labels[labels[i]])
            axs[i].set_title(title)
            axs[i].axes.get_xaxis().set_visible(False)
            axs[i].axes.get_yaxis().set_visible(False)

    axs[i].imshow(img)

plt.show()
